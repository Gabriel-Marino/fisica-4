# Física 4

*Notas de aula do curso de Física 4 - DFI/UEM*

Vamos usar os livros:
- [**Curso de Física Básica: Eletromagnetismo. Vol. 3. Nussenzveig, Herch Moysés.**](http://93.174.95.29/main/5E5DC6B6ABE22DE9998530A0667656BD) 
- [**Curso de Física Básica: Ótica, Relatividade e Física Quântica. Vol. 4. Nussenzveig, Herch Moysés.**](http://libgen.gs/ads.php?md5=5C31BE82F1B7337587FE68FF9CE86721)

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/fisica_iv.pdf).


## Parte 1 - Circuitos Elétricos, Equações de Maxwell, Óptica Geométrica

- [Aula 01 - 18/01/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula01.png)
- [Aula 02 - 20/01/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula02.png)
- [Aula 03 - 25/01/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula03.png)
- [Aula 04 - 01/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula04.png)
- [Aula 05 - 03/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula05.png)
- [Aula 06 - 08/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula06.png)
- [Aula 07 - 08/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula07.png)
- [Aula 08 - 15/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula08.png)
- [Aula 09 - 17/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula09.png)
- [Aula 10 - 22/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula10.png)
- [Aula 11 - 24/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula11.png)

## Parte 2 - Interferência, Difração, Relatividade Restrita, Mecânica Quântica

- [Aula 12 - 01/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula12.png)
- [Aula 13 - 03/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula13.png)
- [Aula 14 - 08/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula14.png)
- [Aula 15 - 10/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula15.png)
- [Aula 16 - 15/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula16.png)
- [Aula 17 - 17/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula17.png)
- [Aula 18 - 22/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula18.png)
- [Aula 19 - 29/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula19.png)
- [Aula 20 - 31/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula20.png)
- [Aula 21 - 05/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula21.png)
- [Aula 22 - 07/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula22.png)
- [Aula 23 - 12/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte2/aula23.png)